$(document).ready(function() {
    $.ajax({
        url: '/ajax/whois/dns',
        method: 'POST',
        data: {"domain": domain}
    }).done(
        function (e) {
            $('#ajaxDns').html(e.renderedResult);
        }
    ).fail(function(e) {
        $('#ajaxDns').html('Something went wrong, please try again later');
    });
    $.ajax({
        url: '/ajax/whois/host',
        method: 'POST',
        data: {"domain": domain}
    }).done(
        function (e) {
            $('#ajaxHost').html(e.renderedResult);
        }
    ).fail(function(e) {
        $('#ajaxHost').html('Something went wrong, please try again later');
    });
    $.ajax({
        url: '/ajax/whois/whois',
        method: 'POST',
        data: {"domain": domain}
    }).done(
        function (e) {
            $('#ajaxWhois').html(e.renderedResult);
        }
    ).fail(function(e) {
        $('#ajaxWhois').html('Something went wrong, please try again later');
    });
});