$(document).ready(function() {
    $('.ninja').show();
    $('.js-disabled').hide();
    $('.js-enabled').show();

    $('#resolution').html(screen.width + ' x ' + screen.height);
    printViewPortSize();

    $(window).resize(function() {
        printViewPortSize();
    });

    function printViewPortSize() {
        $('#viewport').html(window.innerWidth + ' x ' + window.innerHeight);
    }

    $.ajax({
        url: ipv4_ajax_url
    }).done(
        function (e) {
            $('#ipv4 .ip').html('<a href="/whois/?domain='+e.ip+'">'+e.ip+'</a>');

            if(e.country != null) {
                $('#ipv4 .flag').addClass('flag-icon-'+e.country);
            }
        }
    ).fail(function(e) {
        $('#ipv4').html('No ipv4 address found');
    });
    $.ajax({
        url: ipv6_ajax_url
    }).done(
        function (e) {
            $('#ipv6 .ip').html('<a href="/whois/?domain='+e.ip+'">'+e.ip+'</a>');

            if(e.country != null) {
                $('#ipv6 .flag').addClass('flag-icon-'+e.country);
            }
        }
    ).fail(function(e) {
        $('#ipv6').html('No ipv6 address found');
    });

});