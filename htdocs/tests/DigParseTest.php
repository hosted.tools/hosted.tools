<?php

namespace App\Tests;

use App\Service\ParseDigResultService;
use PHPUnit\Framework\TestCase;

class DigParseTest extends TestCase
{
    public function testDigParser()
    {
        $router = $this->createMock('Symfony\Component\Routing\Generator\UrlGeneratorInterface');
        $parseDigResultService = new ParseDigResultService($router);
        $resultToParse = 'hosted.tools.		300	IN	A	127.0.0.1';
        $parsedResult = $parseDigResultService->parse($resultToParse);
        $expectedValue = '127.0.0.1';

        $this->assertEquals($expectedValue, $parsedResult['hosted.tools'][0]['value']);

        $resultToParse = 'hosted.tools.		300	IN	MX	1 mail.hosted.tools.';
        $parsedResult = $parseDigResultService->parse($resultToParse);
        $expectedValue = 'mail.hosted.tools.';

        $this->assertEquals($expectedValue, $parsedResult['hosted.tools'][0]['value']);

        $resultToParse = 'hosted.tools.		86400	IN	NS	this.is.a.nameserver.record.';
        $parsedResult = $parseDigResultService->parse($resultToParse);
        $expectedValue = 'this.is.a.nameserver.record.';

        $this->assertEquals($expectedValue, $parsedResult['hosted.tools'][0]['value']);
    }
}
