<?php

namespace App\Tests;

use App\Service\UserAgentService;
use PHPUnit\Framework\TestCase;

class ParseUserAgentTest extends TestCase
{
    public function testUserAgentParser(): void
    {
        $requestStack = $this->createMock('Symfony\Component\HttpFoundation\RequestStack');
        $userAgentService = new UserAgentService($requestStack);

        // Test with a Chrome 91 on Linux
        $userAgent = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36
        (KHTML, like Gecko) Chrome/91.0.4472.164 Safari/537.36';
        $userAgentService->setUserAgent($userAgent);

        $userAgentInfo = $userAgentService->parseUserAgent();

        $this->assertEquals($userAgentInfo['user_agent'], $userAgent);
        $this->assertEquals($userAgentInfo['OS'], 'Linux');
        $this->assertEquals($userAgentInfo['browser'], 'Chrome 91.0.4472');

        // Test with Edge 17 on Windows 10
        $userAgent = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36
        (KHTML, like Gecko) Chrome/64.0.3282.140 Safari/537.36 Edge/17.17134';
        $userAgentService->setUserAgent($userAgent);

        $userAgentInfo = $userAgentService->parseUserAgent();

        $this->assertEquals($userAgentInfo['user_agent'], $userAgent);
        $this->assertEquals($userAgentInfo['OS'], 'Windows 10');
        $this->assertEquals($userAgentInfo['browser'], 'Edge 17.17134');

        // Test with Firefox 90 on Ubuntu
        $userAgent = 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:90.0) Gecko/20100101 Firefox/90.0';
        $userAgentService->setUserAgent($userAgent);

        $userAgentInfo = $userAgentService->parseUserAgent();

        $this->assertEquals($userAgentInfo['user_agent'], $userAgent);
        $this->assertEquals($userAgentInfo['OS'], 'Ubuntu');
        $this->assertEquals($userAgentInfo['browser'], 'Firefox 90.0');

        // Test with Safari 14 on Mac
        $userAgent = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_6) AppleWebKit/605.1.15
        (KHTML, like Gecko) Version/14.0.3 Safari/605.1.15';
        $userAgentService->setUserAgent($userAgent);

        $userAgentInfo = $userAgentService->parseUserAgent();

        $this->assertEquals($userAgentInfo['user_agent'], $userAgent);
        $this->assertEquals($userAgentInfo['OS'], 'Mac OS X 10 15.6');
        $this->assertEquals($userAgentInfo['browser'], 'Safari 14.0.3');

        // Test with Safari 14 on iOS
        $userAgent = 'Mozilla/5.0 (iPhone; CPU iPhone OS 14_2 like Mac OS X) AppleWebKit/605.1.15
        (KHTML, like Gecko) Version/14.0.1 Mobile/15E148 Safari/604.1';
        $userAgentService->setUserAgent($userAgent);

        $userAgentInfo = $userAgentService->parseUserAgent();

        $this->assertEquals($userAgentInfo['user_agent'], $userAgent);
        $this->assertEquals($userAgentInfo['OS'], 'iOS 14 2');
        $this->assertEquals($userAgentInfo['browser'], 'Mobile Safari UI/WKWebView');
    }
}
