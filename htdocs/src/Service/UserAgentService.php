<?php

namespace App\Service;

use Symfony\Component\HttpFoundation\RequestStack;
use UAParser\Parser;

/**
 * Class UserAgentService
 * @package App\Service
 */
class UserAgentService
{
    /**
     * @var String
     */
    private string $userAgent;

    /**
     * UserAgentService constructor.
     * @param RequestStack $requestStack
     */
    public function __construct(RequestStack $requestStack)
    {
        $userAgent = (!empty($requestStack->getCurrentRequest()->headers)) ?
            $requestStack->getCurrentRequest()->headers->get('User-Agent') : '';
        $this->userAgent = (is_null($userAgent) ? '' : $userAgent);
    }

    public function parseUserAgent()
    {
        $UAParser = Parser::create();
        $info = $UAParser->parse($this->userAgent);

        $clientInfo['user_agent'] = $this->userAgent;

        $clientInfo['OS'] = $info->os->family;
        if (null !== $info->os->major) {
            $clientInfo['OS'] .= ' ' . $info->os->major;
            if (null !== $info->os->minor) {
                $clientInfo['OS'] .= ' ' . $info->os->minor;
                if (null !== $info->os->patch) {
                    $clientInfo['OS'] .= '.' . $info->os->patch;
                    if (null !== $info->os->patchMinor) {
                        $clientInfo['OS'] .= '.' . $info->os->patchMinor;
                    }
                }
            }
        }

        $clientInfo['browser'] = $info->ua->family;
        if (null !== $info->ua->major) {
            $clientInfo['browser'] .= ' ' . $info->ua->major;
            if (null !== $info->ua->minor) {
                $clientInfo['browser'] .= '.' . $info->ua->minor;
                if (null !== $info->ua->patch) {
                    $clientInfo['browser'] .= '.' . $info->ua->patch;
                }
            }
        }
        return $clientInfo;
    }

    /**
     * @return String
     */
    public function getUserAgent(): string
    {
        return $this->userAgent;
    }

    /**
     * @param String $userAgent
     */
    public function setUserAgent(string $userAgent): void
    {
        $this->userAgent = $userAgent;
    }
}
