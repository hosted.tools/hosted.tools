<?php

namespace App\Service;

use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * Class ParseDigResultService
 * @package App\Service
 */
class ParseDigResultService
{
    /**
     * @var UrlGeneratorInterface
     */
    private $router;

    /**
     * ParseDigResultService constructor.
     * @param UrlGeneratorInterface $router
     */
    public function __construct(UrlGeneratorInterface $router)
    {
        $this->router = $router;
    }

    /**
     * @param String $digResult
     * @return String[]
     */
    public function parse(string $digResult): array
    {

        $parsedResult = [];
        if (empty($digResult)) {
            return $parsedResult;
        }

        $dnsRecords = preg_split("/((\r?\n)|(\r\n?))/", $digResult);
        if (!$dnsRecords) {
            return $parsedResult;
        }

        foreach ($dnsRecords as $dnsRecord) {
            if (empty($dnsRecord)) {
                continue;
            }
            $pattern = '/^((.*)\.(?:\ |\t|)+([0-9]+)(?:\ |\t)+IN(?:\ |\t)+([A-Z]+)(?:\ |\t)+(?:[0-9]+\ )?)(.*)$/';
            preg_match($pattern, $dnsRecord, $matches);

            if (!empty($matches) && $matches[2] && $matches[4]) {
                if (empty($parsedResult[$matches[2]])) {
                    $parsedResult[$matches[2]] = [];
                }

                if (preg_match('/[\<|\>]/', $matches[5])) {
                    $matches[5] = htmlspecialchars($matches[5]);
                }

                if ('TXT' === $matches[4] && preg_match('/^(\")?v\=spf/', $matches[5])) {
                    $pattern = '/\:(.+?)\ /';
                    $url = $this->router->generate('whois', ['domain' => ''], UrlGeneratorInterface::ABSOLUTE_URL);
                    $replacement = ":<a href=\"$url$1\">$1</a> ";
                    $matches[5] = preg_replace($pattern, $replacement, $matches[5]);
                }

                $recordToAdd = ['dnsInfo' => $matches[1], 'value' => $matches[5]];
                $parsedResult[$matches[2]][] = $recordToAdd;
            }
        }
        return $parsedResult;
    }
}
