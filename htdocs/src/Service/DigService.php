<?php

namespace App\Service;

use Symfony\Component\Process\Process;

/**
 * Class DigService
 * @package App\Service
 */
class DigService
{
    /**
     * @var String $nameserver
     */
    private $nameserver;

    /**
     * DigService constructor.
     * @param String $nameserver
     * @return void
     */
    public function __construct(string $nameserver)
    {
        $this->nameserver = $nameserver;
    }

    /**
     * @param string $hostname
     * @param string $type
     * @return string
     */
    public function getRecords(string $hostname, string $type = 'A'): string
    {
        $hostname = $this->sanitizeHostname($hostname);

        $command = [
            'dig',
            '+nocmd',
            $hostname,
            $type,
            '+multiline',
            '+noall',
            '+answer',
            '+noidnout',
        ];

        if (!empty($this->nameserver)) {
            $command[] = '@' . $this->nameserver;
        }

        $process = new Process($command);
        $process->setTimeout(2);

        $process->run();
        if ($process->isSuccessful()) {
            return $process->getOutput();
        }
        return '';
    }

    /**
     * @param string $hostname
     * @return string
     */
    private function sanitizeHostname(string $hostname): string
    {
        $hostname = str_replace(['http://', 'https://'], '', $hostname);
        $hostname = strtok($hostname, '/');
        if (!$hostname) {
            return '';
        }

        if (is_string($hostname)) {
            $hostname = (string)idn_to_ascii($hostname);
        }

        return $hostname;
    }
}
