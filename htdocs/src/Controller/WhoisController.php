<?php

namespace App\Controller;

use App\Form\WhoisType;
use App\Service\DigService;
use App\Service\ParseDigResultService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Process\Process;
use Symfony\Component\Routing\Annotation\Route;

class WhoisController extends AbstractController
{
    #[Route('/whois', name: 'whois')]
    public function index(Request $request)
    {
        $queryDomain = $request->query->get('domain');

        $whoisForm = new WhoisType();
        $form = $this->createForm(WhoisType::class, $whoisForm);

        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            $formData = $form->getData();
            $queryDomain = $formData->getDomain();
        }

        $title = 'Whois';
        if (!empty($queryDomain)) {
            $title .= ' ' . $queryDomain;
        }

        return $this->render('whois/index.html.twig', [
            'domain' => $queryDomain,
            'title' => $title,
            'form' => $form->createView(),
        ]);
    }

    #[Route('/ajax/whois/dns', name: 'whoisAjaxDns')]
    public function whoisAjaxDns(Request $request, ParseDigResultService $parseDigResultService, DigService $digService)
    {
        $parameters = $request->request->all();
        $responseData = [];
        if (!array_key_exists('domain', $parameters) || "" === $parameters['domain']) {
            return $this->json($responseData);
        }
        $queryDomain = $parameters['domain'];

        $dnsTypes = ['A', 'AAAA', 'CAA', 'MX', 'NS', 'TXT'];
        $wwwLookup = ['A', 'AAAA'];
        $dnsRecords = [];
        if (null !== $queryDomain) {
            if (!filter_var($queryDomain, FILTER_VALIDATE_IP)) {
                foreach ($dnsTypes as $type) {
                    $domains = [$queryDomain];

                    if (!preg_match('/^www/', $queryDomain) && in_array($type, $wwwLookup)) {
                        $domains[] = 'www.' . $queryDomain;
                    }

                    if (!preg_match('/^mail/', $queryDomain)) {
                        $domains[] = 'mail.' . $queryDomain;
                    }

                    $domains[] = 'thisisawildcardrecord.' . $queryDomain;

                    foreach ($domains as $domain) {
                        try {
                            $result = $digService->getRecords($domain, $type);
                            if (empty($result)) {
                                continue;
                            }
                            $parsedResult = $parseDigResultService->parse($result);

                            if (!isset($dnsRecords[$type])) {
                                $dnsRecords[$type] = $parsedResult;
                            } else {
                                $dnsRecords[$type] = array_merge($dnsRecords[$type], $parsedResult);
                            }
                        } catch (\Exception $e) {
                        }
                        try {
                            // Special cases
                            $result = $digService->getRecords('_dmarc.' . $domain, 'TXT');
                            if (!empty($result)) {
                                $parsedResult = $parseDigResultService->parse($result);

                                if (!isset($dnsRecords['DMARC'])) {
                                    $dnsRecords['DMARC'] = $parsedResult;
                                } else {
                                    $dnsRecords['DMARC'] = array_merge($dnsRecords['DMARC'], $parsedResult);
                                }
                            }
                        } catch (\Exception $e) {
                        }
                    }
                }
            }
        }

        ksort($dnsRecords);

        $responseData['dnsRecords'] = $dnsRecords;

        $responseData['renderedResult'] = $this->render('whois/ajax/dns.html.twig', [
            'dnsRecords' => $dnsRecords
        ])->getContent();

        return $this->json($responseData);
    }

    #[Route('/ajax/whois/host', name: 'whoisAjaxHost')]
    public function whoisAjaxHost(Request $request)
    {
        $parameters = $request->request->all();
        $responseData = [];
        if (!array_key_exists('domain', $parameters) || "" === $parameters['domain']) {
            return $this->json($responseData);
        }
        $queryDomain = $parameters['domain'];

        $host = '';
        if (null !== $queryDomain) {
            $process = new Process(['host', $queryDomain]);
            $process->run();
            if ($process->isSuccessful()) {
                $host = $process->getOutput();
            }
        }

        $responseData['renderedResult'] = $this->render('whois/ajax/host.html.twig', [
            'host' => $host
        ])->getContent();

        return $this->json($responseData);
    }

    #[Route('/ajax/whois/whois', name: 'whoisAjaxWhois')]
    public function whoisAjaxWhois(Request $request)
    {
        $parameters = $request->request->all();
        $responseData = [];
        if (
            !array_key_exists('domain', $parameters) ||
            "" === $parameters['domain'] ||
            null === $parameters['domain']
        ) {
            return $this->json($responseData);
        }
        $queryDomain = $parameters['domain'];

        $whoisData = '';
        $process = new Process(['whois', $queryDomain]);
        $process->setTimeout(2);
        $process->run();

        $whoisData = '';
        if ($process->isSuccessful()) {
            $whoisData = trim($process->getOutput());
        }

        $responseData['renderedResult'] = $this->render('whois/ajax/whois.html.twig', [
            'whoisData' => $whoisData
        ])->getContent();

        return $this->json($responseData);
    }
}
