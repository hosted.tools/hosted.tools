<?php

namespace App\Controller;

use App\Service\UserAgentService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use UAParser\Exception\FileNotFoundException;

class ClientInfoController extends AbstractController
{
    #[Route('/', name: 'client_info')]
    public function index(UserAgentService $userAgentService)
    {
        $clientInfo = [];

        $userAgentInfo = $userAgentService->parseUserAgent();
        if (!empty($userAgentInfo) && is_array($userAgentInfo)) {
            $clientInfo = array_merge($clientInfo, $userAgentInfo);
        }

        return $this->render('client_info/index.html.twig', [
            'client_info' => $clientInfo,
        ]);
    }

    #[Route('/ip', name: 'ip')]
    public function ip(Request $request)
    {
        $ip = $request->server->get('REMOTE_ADDR');

        if ($request->server->get('HTTP_X_FORWARDED_FOR')) {
            $ip = $request->server->get('HTTP_X_FORWARDED_FOR');
        }
        if ($request->server->get('HTTP_CF_CONNECTING_IP')) {
            $ip = $request->server->get('HTTP_CF_CONNECTING_IP');
        }

        if ($request->query->get('format') === 'json') {
            return $this->json(['ip' => $ip]);
        }
        return $this->render('client_info/ip.html.twig', [
            'ip' => $ip,
        ]);
    }
}
